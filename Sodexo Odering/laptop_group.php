<!DOCTYPE html>
<html lang="en">
<title>Sodexo Menu</title>
<meta charset="utf-8">
<style>
h1 {
	font: 16px Helvetica, sans-serif;
	//display:table-cell;
	text-align: left;
	margin-left: 1.4%;
	line-height: 33px;
	color: white;
}
h2 {
	font: 14px Helvetica, sans-serif;
	text-align: left;
	margin-left: 20px;
	color: #598200;
	line-height: 0px;
	

}
h3 {
	font: 15px Helvetica, sans-serif;	
	text-align: left;

	color: #666666;

	position: relative;
	top: -3px;
		
}
h4 {
	font: 16px Helvetica, sans-serif;	
	text-align: center;
	color: #666666;
	line-height: 1px;

}
.label1{
    display: inline-block;
    float: left;
    clear: left;
    width: 40px;
    text-align: right;
}
.input1 {
  display: inline-block;
  float: left;
}
a:link {
	font: 15px Helvetica, sans-serif;	
	text-align: left;
	color: #777777;
	line-height: 5px;
	text-decoration: none;
}
.myBtn
{
	font: 18px Helvetica, sans-serif;	
	color: #ffffff;li
	font-size: 19px;
	background: #598200;
	padding: 5px 20px 5px 20px;
	text-decoration: none;
	border:none;

}
.myBtn1
{
	font: 13px Helvetica, sans-serif;	
	color: #ffffff;li
	font-size: 19px;
	background: #598200;
	padding: 5px 20px 5px 20px;
	text-decoration: none;
	border:none;

}
.myBox1
{
	width:80px;
	height:22px;
}

.menu_frame {
	position: absolute;
	top:50px;
	left:50%;
    width: 1000px;
	transform: translate(-50%, 0);
}
.menu_head {
    background-color: #598200;
	height:33px;

}
.menu_body {
	//display: table;
	border-style: solid;
    border-width: 3px;
	border-color: #598200;
}
.menu_item {
	float:left;
	height:95px;
}
.menu_item_right {
	width:90px;
	margin-left:120px;
	position:relative;
	top: -10px;
}
.quantityBox
{
	width:40px;
}
.text_section
{
	width:95%;
	margin:0 auto;
}
</style>

<body>
	<div class = "menu_frame">
		<a href="Home.php">Home</a>&nbsp&nbsp <a href="Profile.php">Profile</a>
		<hr size=1>
		<div class = "menu_head">
			<h1>Sodexo Menu</h1>
		</div>
		<div class = "menu_body">
			<br>
			<?php
			include 'connection.php';
			// Create connection to Oracle
			 $conn = connect();
			 
			if (!$conn) {
			   $m = oci_error();
			   echo $m['message'], "\n";
			   exit;
			}
			else {
				session_start();
				echo "<div class=\"text_section\">";
				echo "<h3>";
				echo "<form method=\"post\" action=\"laptop_group.php\">"
				   ."Narrow down Comment type: " 
					."<select name=\"narrowdown\" class=\"myBox1\" >"
					 ."<option value=\"all\">All</option>"
					  ."<option value=\"Others\">Others</option>"
					  ."<option value=\"Breakfast\">Breakfast</option>"
					  ."<option value=\"Lunch\">Lunch</option>"
					  ."<option value=\"Drink\">Drink</option>"
					  ."<option value=\"Dessert\">Dessert</option>"
					  ."<select><input type=\"submit\" class=\"myBtn1\" style=\"margin-left:10px\" value=\"Narrow\"></form><hr>";
				$sql="select comments.comm_id,comm_type,content,username,brand,model,date_comm from comments,laptop"
				 ." where comments.lap_id=laptop.lap_id and comm_type<>'completed' ORDER BY date_comm DESC,comments.comm_id DESC ";
		
				$getcomments=oci_parse($conn,$sql);
				oci_execute($getcomments);
	
				$typearray=array("Breakfast","Lunch","Drink","Dessert","Others");
	
				if(isset($_POST['narrowdown']))
				 {
				   if($_POST['narrowdown']=="all")
				   $typearray=array("Breakfast","Lunch","Drink","Dessert","Others");
				   else
				   $typearray=array($_POST['narrowdown']);
				 }
				 
				while($row=oci_fetch_array($getcomments,OCI_ASSOC))
				{
				   if(in_array($row['COMM_TYPE'],$typearray))
				   {
				   echo  "Type: ".$row['COMM_TYPE']."<br>Name: ".$row['BRAND'] ."<br>";

				   $sql2="select price from prices where comm_id=".$row['COMM_ID'];
				   $pricequery=oci_parse($conn,$sql2);
				   oci_execute($pricequery);
				   $price=oci_fetch_array($pricequery,OCI_ASSOC);
				   echo "Price: $".$price['PRICE'].".00<br>";

	
				   echo "Description: ".$row['CONTENT'];
					if($_SESSION['admin']==1)
					   echo "<form method=\"post\" action=\"report.php\">"
						."<input type=\"hidden\" name=\"comm_id\" value=\"".$row['COMM_ID']."\">"
					   ." <br><input type=\"submit\" class=\"myBtn1\" value=\"Delete\"></form>";

				   echo "<form method=\"post\" action=\"addToCart.php\">"
				   ."<input type=\"hidden\" name=\"comm_id\" value=\"".$row['COMM_ID']."\">"
				   ."<input type=\"hidden\" name=\"price\" value=\"".$price['PRICE']."\">"
				   ."<input type=\"hidden\" name=\"brand\" value=\"".$row['BRAND']."\">"
				   ."<input type=\"hidden\" name=\"seller\" value=\"".$row['USERNAME']."\">"
				   ."<br>Quantity:  <input type=\"number\" min=\"1\" name=\"quantity\" class=\"quantityBox\" value=\"1\">"
				   ."<br><br><input type=\"submit\" class=\"myBtn\" value=\"Add to cart\">"
				   ."</form><br>";

				   
				   $sqlcc="select cc_id,rate,content,username,date_comm from comm_on_comm"
					  ." where comm_id=".$row['COMM_ID'];
				   $cc=oci_parse($conn,$sqlcc);
				   oci_execute($cc);
				   
				  while(($ccrow=oci_fetch_array($cc,OCI_ASSOC))!=false)
				  {
					  echo "<form method=\"post\" action=\"ratecomment.php\">"
						   ."Rated: ".$ccrow["RATE"].", ".$ccrow["CONTENT"]." ---".$ccrow["USERNAME"]." ".$ccrow["DATE_COMM"]
						   ."<input type=\"hidden\" name=\"cc_id\" value=\"".$ccrow['CC_ID']."\"></form>";

						   
				  }
				  
				  echo "<form method=\"post\" action=\"ratecomment.php\">"
					   ."<br>Rate it <input type=\"number\" min=\"0\" max=\"5\" name=\"rate\">" 
					   ." Comment <input type=\"text\"  name=\"content\">"
					   ."<input type=\"hidden\" name=\"comm_id\"  value=\"".$row['COMM_ID']."\">"
					   ."   <input type=\"Submit\" style=\"margin-left:5px\" class=\"myBtn1\" value=\"submit\">"
					   ."</form>";
				 
				   echo "<br><hr>";
				   }//end first if in while
				}//end first while
				oci_free_statement($getcomments);

				echo "<form method=\"post\" action=\"transaction.php\">"
					."<input type=\"hidden\" name=\"seller\" value=\"".$row['USERNAME']."\">"
				   ."<input type=\"hidden\" name=\"seller\" value=\"".$row['USERNAME']."\">"
				   ."<br><br><input type=\"submit\" class=\"myBtn\" value=\"Check out\">"
				   ."</form><br>";

				if($_SESSION['admin']==1){
				  $lapidquery="select lap_id,brand,model from laptop";
				  $lapid=oci_parse($conn,$lapidquery);
				  oci_execute($lapid);

				   echo "<br><form method=\"post\" action=\"addcomment.php\">"
				   	."<b style=\"font-size:18px\">Add new items</b><br><br>"
					."<label class=\"label1\">Name: </label><select name=\"lapid\" class=\"input1\" required=\"required\">";
				   while(($row=oci_fetch_array($lapid,OCI_ASSOC))!=false)
				   {
					   echo "<option value=\"".$row['LAP_ID']."\">".$row['BRAND'] ."</option>";
				   }
				   echo "</select >(If there is no name for the new item, click <a href=\"addlaptop.php\">here<a> to add it.)<br>"
					."<br><label class=\"label1\">Type: </label><select name=\"comm_type\" class=\"input1\" required=\"required\">"
					."<option value=\"Others\">Others</option>"
					."<option value=\"Breakfast\">Breakfast</option>"
					."<option value=\"Lunch\">Lunch</option>"
					."<option value=\"Drink\">Drink</option>"
					."<option value=\"Dessert\">Dessert</option>"
					."</select>"
				   ."<br><br><label class=\"label1\">Price:</label><input type=\"number\" min=\"0\" class=\"input1\" name=\"price\" required=\"required\"> <br>"
				   ."<br><textarea name=\"content\" cols=\"30\" rows=\"4\" placeholder=\"Enter your description...\" required=\"required\"></textarea><br>"
				   ."<input type=\"hidden\" name=\"lap_id\" value=\"".$row['LAP_ID']."\">"
				   ." <br><input type=\"submit\" class=\"myBtn1\" value=\"Submit\">"
				   ."</form>";
	
				}}
			echo "</h3>";
			echo "</div>";
			// Close the Oracle connection
			oci_close($conn);
			
			?>
			
		</div>
	<br>
	<br>
	<br>
	<br>
	</div>


</body>


</html>
