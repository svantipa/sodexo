<!DOCTYPE html>
<html lang="en">
<title>Sodexo Menu</title>
<meta charset="utf-8">
<style>
h1 {
	font: 16px Helvetica, sans-serif;
	//display:table-cell;
	text-align: left;
	margin-left: 1.4%;
	line-height: 33px;
	color: white;
}
h2 {
	font: 14px Helvetica, sans-serif;
	text-align: left;
	margin-left: 20px;
	color: #598200;
	line-height: 0px;
	

}
h3 {
	font: 15px Helvetica, sans-serif;	
	text-align: left;
	color: #666666;
	line-height:20px;

		
}
h4 {
	font: 16px Helvetica, sans-serif;	
	text-align: center;
	color: #666666;
	line-height: 1px;

}
.label1{
    display: inline-block;
    float: left;
    clear: left;
    width: 40px;
    text-align: right;
}
.input1 {
  display: inline-block;
  float: left;
}
a:link {
	font: 15px Helvetica, sans-serif;	
	text-align: left;
	color: #777777;
	line-height: 5px;
	text-decoration: none;
}
.myBtn
{
	font: 18px Helvetica, sans-serif;	
	color: #ffffff;
	font-size: 19px;
	background: #598200;
	padding: 5px 20px 5px 20px;
	text-decoration: none;
	border:none;

}
.myBtn1
{
	font: 13px Helvetica, sans-serif;	
	color: #ffffff;
	font-size: 19px;
	background: #598200;
	padding: 5px 20px 5px 20px;
	text-decoration: none;
	border:none;

}
.myBox1
{
	width:80px;
	height:22px;
}

.menu_frame {
	position: absolute;
	top:50px;
	left:50%;
    width: 1000px;
	transform: translate(-50%, 0);
}
.menu_head {
    background-color: #598200;
	height:33px;

}
.menu_body {
	//display: table;
	border-style: solid;
    border-width: 3px;
	border-color: #598200;
}
.menu_item {
	float:left;
	height:95px;
}
.menu_item_right {
	width:90px;
	margin-left:120px;
	position:relative;
	top: -10px;
}
.quantityBox
{
	width:40px;
}
.text_section
{
	width:95%;
	height:95%;
	margin:0 auto;
}
</style>

<body>
<div class = "menu_frame">
	<a href="Home.php">Home</a>&nbsp&nbsp <a href="Profile.php">Profile</a>
	<hr size=1>
	<div class = "menu_head">
		<h1>Cart</h1>
	</div>
	<div class = "menu_body">
		<div class ="text_section">
			<h3>
			<?php
			   include 'connection.php';
			   $conn=connect();
			   session_start();
                                        $sqlq1="insert into shopping_cart values (:username,:brand,:quantity,:price, :comm_id)";
                                        
					$result=oci_parse($conn,$sqlq1);
                                        oci_bind_by_name($result, ':username',$_SESSION['user']);
                                        oci_bind_by_name($result, ':brand',$_POST['brand']);
                                        oci_bind_by_name($result, ':quantity',$_POST['quantity']);
                                        oci_bind_by_name($result, ':price',$_POST['price']);
										oci_bind_by_name($result, ':comm_id',$_POST['comm_id']);
					oci_execute($result);
					echo "Added to cart<br>";
					echo "Click <a href=\"laptop_group.php\">here </a> to go back."
	
				//call laptop group page from here
			?>
			</h3>
		</div>
	</div>

</body>
</html>
