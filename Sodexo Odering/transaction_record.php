<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Transaction records</title>
</head>
<h1>Transaction Records</h1>
<?php 
include 'connection.php';
$conn=connect();

$username="lifeng";

$sql="select buyer,seller,payment,date_tran,brand,model  from transaction,comments,laptop"
      ." where (buyer=:myusername or seller=:myusername) AND comments.lap_id=laptop.lap_id"
	  ." AND transaction.comm_id=comments.comm_id";
$result=oci_parse($conn,$sql);

oci_bind_by_name($result,':myusername',$username);
oci_execute($result);

$order=1;
while(($row=oci_fetch_array($result,OCI_ASSOC))!=false)
{
	if($row['BUYER']=$username)
	echo $order.". buy ".$row['BRAND'].$row['MODEL']." at $".$row['PAYMENT'].", Date:".$row['DATE_TRAN']."<BR/>";
	else
	echo $order.". sell ".$row['BRAND'].$row['MODEL']." at $".$row['PAYMENT'].", Date:".$row['DATE_TRAN']."<BR/>";
	echo "<hr/>";
	$order+=1;
}
oci_free_statement($result);
?>
<body>
</body>
</html>
